// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

// 7.0.3
let package = Package(
    name: "FreestarAds-Vungle",
     platforms: [
        .iOS(.v11),
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "FreestarAds-Vungle",
            targets: [
                "FreestarAds-Vungle",
                "FreestarAds-Vungle-Core",
                "VungleAdsSDK"
                ]
            )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(
            url: "https://gitlab.com/freestar/spm-freestarads-core.git",
            from: "5.17.0"
            )
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .binaryTarget(
            name: "FreestarAds-Vungle",
            url: "https://gitlab.com/freestar/spm-freestarads-vungle/-/raw/7.0.3/FreestarAds-Vungle.xcframework.zip",
            checksum: "b4ca964a3ae8d18d21cc51bd0ea224abb911add27ac82e37e57457445a4deca7"
            ),
        .target(
            name: "FreestarAds-Vungle-Core",
            dependencies: [
                .product(name: "FreestarAds", package: "spm-freestarads-core")
            ]
            ),
        .binaryTarget(
            name: "VungleAdsSDK",
            url: "https://gitlab.com/freestar/spm-freestarads-vungle/-/raw/7.0.3/VungleAdsSDK.xcframework.zip",
            checksum: "7f09499be7f30ea6d3efedc8ab9703e8c803e4233ce28ae53a36f4c21e5f8592"
        )
    ]
)
